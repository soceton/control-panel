# Control Panel for managing Django/Flask in cloud instances
Host single/multiple Django/Flask website on your cloud server. You can host multiple domain and sub-domain if you follow this process
and it will reduce time and complexity of enabling hosts on Linux based systems.


## Prerequisites

    * NGINX Server
    * Virtual Environment

## Instruction
Discussion on installation with example. 

Project directory - `/home/ubuntu/soceton.com/ajax`

Domain Name - `ajax.soceton.com`

#### Run these commands to make ready Django Project. This for basic project installation, if you need more to change to make then make sure have done that.
    
```shell
panel@soceton:~$ cd /home/ubuntu/soceton.com/ajax
panel@soceton:~/soceton.com/ajax$ python3 -m venv env
panel@soceton:~/soceton.com/ajax$ source env/bin/activate
panel@soceton:~/soceton.com/ajax$ pip3 install django
panel@soceton:~/soceton.com/ajax$ pip3 install gunicorn
panel@soceton:~/soceton.com/ajax$ deactivate
```
    
It will create a virtual environment in the project root directory. Here, `/home/ubuntu/soceton.com/ajax/env`

#### Clone our control panel wherever you want want. For this example I am cloning it in user home directory
    
```shell
panel@soceton:~$ git clone https://soceton@bitbucket.org/soceton/control-panel.git
```
   
#### Run these commands to create files that you will need to run a host

```shell
panel@soceton:~$ cd control-panel
panel@soceton:~/control-panel$ sudo python3 enable-host.py -d ajax.soceton.com -r /home/ubuntu/soceton.com/ajax
```
   
#### Apply changes to the instance. Replace {{domain}} with your domain name

```shell
panel@soceton:~$ sudo systemctl start ajax.soceton.com.socket 
panel@soceton:~$ sudo systemctl enable ajax.soceton.com.socket 
panel@soceton:~$ sudo systemctl restart nginx 
```

#### For restarting the host. Replace {{domain}} with your domain name.

```shell
panel@soceton:~$ sudo systemctl restart ajax.soceton.com
```

If you want to host another domain, you may repeat the process with your new domain.
## Arguments

Description | Command | Short | Required | Default
--- | --- | --- | --- | --- 
Description for the host | --description | -ds | False | Domain Name
Domain name | --domain | -d | True | None
Location of the working directory | --root | -r | True | None
Env location | --env | -e | False | ProjectLocation/env
OS username | --user | -u | False | ubuntu
django app name | --app | -a | False | Project name
