import argparse
import re
from os.path import join, basename
from os import symlink, remove


class EnableHost:
    def __init__(self, values):
        self.__values = values
        self.__default_values = {
            'user': "ubuntu",
            'description': self.__values['domain'],
            'env': join(self.__values['root'], "env"),
            'app': basename(self.__values['root'])
        }

        self.write_file(
            'service-file',
            '/etc/systemd/system/{}.service'.format(self.__values['domain'])
        )

        self.write_file(
            'socket-file',
            '/etc/systemd/system/{}.socket'.format(self.__values['domain'])
        )

        self.write_file(
            'host-file',
            '/etc/nginx/sites-available/{}'.format(self.__values['domain'])
        )

        source = '/etc/nginx/sites-available/{}'.format(self.__values['domain'])
        destination = '/etc/nginx/sites-enabled/{}'.format(self.__values['domain'])

        try:
            symlink(source, destination)
        except FileExistsError:
            remove(destination)
            symlink(source, destination)

        print("Host enabled - {} \n\tLocation - {}".format(self.__values["domain"], self.__values["root"]))
        print("Now run below commands to apply changes - \n")
        print("\tsudo systemctl start {}.socket".format(self.__values['domain']))
        print("\tsudo systemctl enable {}.socket".format(self.__values['domain']))
        print("\tsudo systemctl restart nginx\n")

    def prepare_lines(self, example):
        prepared_lines = []
        for line in example:
            regex = r"\{{(.*?)\}}"
            matches = re.finditer(regex, line)

            prepared_line = line
            for matchNum, match in enumerate(matches):
                for groupNum in range(0, len(match.groups())):
                    pattern = r"{{" + match.group(1) + "}}"
                    try:
                        prepared_line = re.sub(pattern, self.__values[match.group(1)], prepared_line)
                    except (KeyError, TypeError) as e:
                        prepared_line = re.sub(pattern, self.__default_values[match.group(1)], prepared_line)

            prepared_lines.append(prepared_line)

        return prepared_lines

    def write_file(self, example_file, location):
        with open("./examples/{}".format(example_file)) as file:
            lines = file.readlines()
            lines = self.prepare_lines(lines)

            with open(location, 'w') as w_file:
                w_file.writelines([str(line) for line in lines])


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='enable sub-domain for django application within cloud instance')

    parser.add_argument('-ds', '--description', help='description')
    parser.add_argument('-d', '--domain', help='domain name', required=True)
    parser.add_argument('-r', '--root', help='location of the working directory', required=True)
    parser.add_argument('-e', '--env', help='env location')
    parser.add_argument('-u', '--user', help='os username')
    parser.add_argument('-a', '--app', help='django app name')

    args = parser.parse_args()
    args = vars(args)

    EnableHost(args)
